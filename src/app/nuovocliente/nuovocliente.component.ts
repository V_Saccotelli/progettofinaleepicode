import { ProvinceComuniService } from './../services/province-comuni.service';
import { ClientiService } from './../services/clienti.service';
import { Component, OnInit } from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Cliente } from '../classes/cliente';

@Component({
  selector: 'app-nuovocliente',
  templateUrl: './nuovocliente.component.html',
  styleUrls: ['./nuovocliente.component.css']
})
export class NuovoclienteComponent implements OnInit {
  model!: NgbDateStruct;
  cliente!:any;
  comuni!:any;
  province!:any;
  tipocliente!:any;

  ragioneSociale!:any;
  partitaIva!:any;
  tipoCliente!:any;
  email!:any;
  pec!:any;
  telefono!:any;
  nomeContatto!:any;
  cognomeContatto!:any;
  telefonoContatto!:any;
  emailContatto!:any;
  via!:any;
  civico!:any;
  cap!:any;
  localita!:any;
  nomecomune!:any;
  nomeprovincia!:any;
  sigla!:any;

  viaSedeLegale!:any;
  civicoSedeLegale!:any;
  capSedeLegale!:any;
  localitaSedeLegale!:any;

  nomecomuneSedeOperativa!:any;
  nomeprovinciaSedeOperativa!:any;
  siglacomuneSedeOperativa!:any;

  nomecomuneSedeLegale!:any;
  nomeprovinciaSedeLegale!:any;
  siglacomuneSedeLegale!:any;

  dataInserimento = new Date();



   

  constructor(private clientiService: ClientiService,
    private comuniprovinceService: ProvinceComuniService) { }

  ngOnInit(): void {
    alert('Assicurati di inserire prima il Comune e poi tutti gli altri dati del cliente')
    this.comuniprovinceService.leggiTutteComuni()
    .subscribe(resp => this.comuni = resp);

    this.comuniprovinceService.leggiTutteProvince()
    .subscribe(resp => this.province = resp)

    this.clientiService.tipiClienti()
    .subscribe(resp => this.tipocliente = resp);
  }

  creaCliente(){
    let obj = (
      {
        ragioneSociale: this.ragioneSociale,
        partitaIva: this.partitaIva,
        tipoCliente: this.tipoCliente,
        email: this.email,
        pec: this.pec,
        telefono: this.telefono,
        nomeContatto: this.nomeContatto,
        cognomeContatto: this.cognomeContatto,
        telefonoContatto: this.telefonoContatto,
        emailContatto: this.emailContatto,
        indirizzoSedeOperativa: {
            via: this.via,
            civico: this.civico,
            cap: this.cap,
            localita: this.localita,
            comune: {
                id: 1,
                nome: this.nomecomune,
                provincia: {
                    id: 1,
                    nome: this.nomeprovinciaSedeOperativa,
                    sigla: this.siglacomuneSedeOperativa
                }
            }
        },
        indirizzoSedeLegale: {
            via: this.viaSedeLegale,
            civico: this.civicoSedeLegale,
            cap: this.capSedeLegale,
            localita: this.localita,
            comune: {
                id: 1,
                nome: this.nomecomuneSedeLegale,
                provincia: {
                    id: 1,
                    nome: this.nomeprovinciaSedeLegale,
                    sigla:this.siglacomuneSedeLegale
                }
            }
        },
        dataInserimento: this.dataInserimento,
        dataUltimoContatto: "2021-03-24T21:32:06.375+00:00" 
    }
    )
    this.clientiService.aggiungiCliente(obj)
    .subscribe(resp => console.log(resp));
  }



}
