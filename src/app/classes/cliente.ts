export class Cliente {
    ragioneSociale!: string;
    partitaIva: string;
    tipoCliente: string;
    email: string;
    pec: string;
    telefono: string;
    nomeContatto: string;
    cognomeContatto: string;
    telefonoContatto: string;
    emailContatto: string;
    indirizzoSedeOperativa: {
        via: string;
        civico: string;
        cap: string;
        localita: string;
        comune: {
            nome: string;
            provincia: {
               
                nome: string;
                sigla: string;
            }
        }
    }
    indirizzoSedeLegale: {
    via: string;
        civico:string;
        cap: string;
        localita:string;
        comune: {
            
            nome: string;
            provincia: {
                
                nome: string;
                sigla: string;
            }
        }
    }

    constructor( ragioneSociale: string,
        partitaIva: string,
        tipoCliente: string,
        email: string,
        pec: string,
        telefono: string,
        nomeContatto: string,
        cognomeContatto: string,
        telefonoContatto: string,
        emailContatto: string,
        indirizzoSedeOperativa:{
            via: string;
                civico:string;
                cap: string;
                localita:string;
                comune: {
                    
                    nome: string;
                    provincia: {
                        
                        nome: string;
                        sigla: string;
                    }
                }
            },

            indirizzoSedeLegale:{
                via: string;
                    civico:string;
                    cap: string;
                    localita:string;
                    comune: {
                        
                        nome: string;
                        provincia: {
                            
                            nome: string;
                            sigla: string;
                        }
                    }
                }


       
        ){

        this.ragioneSociale = ragioneSociale;
        this.partitaIva = partitaIva,
        this.tipoCliente= tipoCliente,
        this.email= email,
        this.pec= pec,
        this.telefono= telefono,
        this.nomeContatto= nomeContatto,
        this.cognomeContatto= cognomeContatto,
        this.telefonoContatto= telefonoContatto,
        this.emailContatto= emailContatto,
        this.indirizzoSedeOperativa = indirizzoSedeOperativa,
        this.indirizzoSedeLegale = indirizzoSedeLegale

    }
}
 