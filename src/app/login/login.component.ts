import { Router } from '@angular/router';
import { RoutguardService } from './../services/routeguard.service';
import { SignuploginService } from './../services/signuplogin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
username!:string;
password!:string;

strigResp!:any;
  constructor(private loginservice: SignuploginService,
    private guard: RoutguardService,
    private router: Router ) { }

  ngOnInit(): void {
  }

  loginUser(){
      let objLog = {
        username: this.username,
        password: this.password
      }   
      
      this.loginservice.login(objLog)
      .subscribe(resp => { 
        console.log(resp);
      this.strigResp = JSON.stringify(resp)
      localStorage.setItem('risposta', this.strigResp)
      //localStorage.getItem(this.strigResp)
      alert('Login Giusto')
      //this.guard.loginApp();
      this.router.navigate(['home']);
      })
  
    
      //this.router.navigate(['home']);
      /* this.loginservice.login(objLog)
      .subscribe(resp => console.log(resp)) */
  }

}
