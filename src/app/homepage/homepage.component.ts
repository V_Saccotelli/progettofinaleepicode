import { FattureService } from './../services/fatture.service';
import { ProvinceComuniService } from './../services/province-comuni.service';
import { Component, OnInit } from '@angular/core';
import { ClientiService } from '../services/clienti.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  clienti!: any;
  province!:any;
  data = new Date();
  fattureTot!: any;
    constructor(private clientiService: ClientiService,
    private fattureService: FattureService,
    private provinceService: ProvinceComuniService) { }

  ngOnInit(): void {
    this.clientiService.leggiTuttiClienti(1).subscribe(resp => this.clienti = resp)
    this.fattureService.leggiTutteFatture(this.fattureTot).subscribe(resp => this.fattureTot = resp)
  }


}
