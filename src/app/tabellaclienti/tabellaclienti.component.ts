import { FattureService } from './../services/fatture.service';
import { ClientiService } from './../services/clienti.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-tabellaclienti',
  templateUrl: './tabellaclienti.component.html',
  styleUrls: ['./tabellaclienti.component.css'],

})
export class TabellaclientiComponent implements OnInit {


  clienti!: any;
  clientsCount!: any;

  constructor(private serviceClienti: ClientiService,
    private fattureService: FattureService,
    private router: Router) { }

  ngOnInit(): void {
    this.serviceClienti.leggiTuttiClienti(0)
      .subscribe(resp => {
        this.clienti = resp
        this.clientsCount = new Array(Math.ceil(this.clienti.totalElements / 10));
      }
      );
  }

  modificaCliente(item: any) {
    this.router.navigate(['cliente', item.id, 'modifica'])
  }

  eliminaCliente(item: any){
      this.serviceClienti.cancellaCliente(item.id)
      .subscribe(resp => console.log(resp));
  }

  getPaginations(pagination:any){
    this.serviceClienti.leggiTuttiClienti(pagination)
    .subscribe(resp => {
      console.log(resp)
      this.clienti = resp
    })
  }

  visualizzaFatture(item: any){
    this.router.navigate(['cliente', item.id, 'fattura'])
  }

}
