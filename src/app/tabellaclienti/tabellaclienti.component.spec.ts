import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabellaclientiComponent } from './tabellaclienti.component';

describe('TabellaclientiComponent', () => {
  let component: TabellaclientiComponent;
  let fixture: ComponentFixture<TabellaclientiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabellaclientiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabellaclientiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
