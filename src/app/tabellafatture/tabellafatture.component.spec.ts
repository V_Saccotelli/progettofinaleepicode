import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabellafattureComponent } from './tabellafatture.component';

describe('TabellafattureComponent', () => {
  let component: TabellafattureComponent;
  let fixture: ComponentFixture<TabellafattureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabellafattureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabellafattureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
