import { FattureService } from './../services/fatture.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-tabellafatture',
  templateUrl: './tabellafatture.component.html',
  styleUrls: ['./tabellafatture.component.css']
})
export class TabellafattureComponent implements OnInit {

  displayedColumns: string[] = ['position', 'Data', 'Numero', 'Importo', 'Stato'];
  dataSource!:any;
  fattureCount!:any;
  constructor(private fattureService: FattureService) { }

  ngOnInit(): void {
    this.fattureService.leggiTutteFatture(0)
    .subscribe(resp => {
      console.log(resp)
      this.dataSource = resp
    });
  }

}
