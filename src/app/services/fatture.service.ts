import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FattureService {

  uriFatture = 'https://epicode.online/epicodebeservice_v2/'
  constructor(private http: HttpClient) { }

  leggiTutteFatture(pagination: number){
   return this.http.get(this.uriFatture + 'api/fatture?page='+pagination+'&size=20&sort=id,ASC');
  }

  leggiFattureCliente(id: number){
  return this.http.get(this.uriFatture + 'api/fatture/cliente/'+id+'?page=0&size=20&sort=id,ASC')
  }

  leggiFatturaById(id: number){
    return this.http.get(this.uriFatture + 'api/fatture/'+id)
  }

  eliminaFattura(id: number){
   return this.http.delete(this.uriFatture+'api/fatture/'+id)

  }

  modificaFattura(item: any){
  return this.http.put(this.uriFatture+'api/fatture/'+item.id, item)
  }

  creaFattura(fattura: any){
    return this.http.post(this.uriFatture + 'api/fatture/', fattura)
  }

  cercaByAnno(anno: number){
    return this.http.get(this.uriFatture+'api/fatture/anno/?anno='+anno+'&page=0&size=20&sort=id,ASC')
  }

  cercaTramiteDate(datauno: any, datadue:any){
    return this.http.get(this.uriFatture + 'api/fatture/data/?from='+datauno+'&to='+datadue+'&page=0&size=20&sort=id,ASC')
  }

  cercaTramiteImporto(primoimporto: any, secondoimporto:any){
    return this.http.get(this.uriFatture+'api/fatture/importo/?from='+primoimporto+'&to='+secondoimporto+'&page=0&size=20&sort=id,ASC')
  }

  leggiStatoFattura(){
    return this.http.get(this.uriFatture + 'api/statifattura')
  }
}
