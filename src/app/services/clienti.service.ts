import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientiService {

  breaer= 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTYyODQ5Nzk1NCwiZXhwIjoxNjI5MzYxOTU0fQ.wTJMz94WPazaNZ5y9StUQzt_BnTx8qCVHsd5dTDBKRvQhtTvZK0n__GZ1ltU3U2c4pKCX-HgHwVUjrKiAHErnw'
  uriApiClienti = 'https://epicode.online/epicodebeservice_v2/api/clienti/'
  uriApiCrea = 'https://epicode.online/epicodebeservice_v2/api/clienti'
  constructor(private http: HttpClient) {
    
   }

  leggiTuttiClienti(pagination:any){
   return this.http.get(this.uriApiClienti+'?page='+pagination+'&size=10&sort=id,ASC');
  }

  leggiClienteById(id: number){
    return this.http.get(this.uriApiClienti+id);

  }

  cancellaCliente(id: number){
   return this.http.delete(this.uriApiClienti+id);
  }

  updateCliente(item: any){
    return this.http.put(this.uriApiClienti+item.id, item)

  }
  aggiungiCliente(cliente: any){
    return this.http.post(this.uriApiCrea, cliente)
  }

  tipiClienti(){
    return this.http.get(this.uriApiClienti+'tipicliente')
  }
}
