import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProvinceComuniService {

  uriApiComuni = 'https://epicode.online/epicodebeservice_v2/api/comuni?page=0&size=20&sort=id,ASC'
  uriApiProvince ='https://epicode.online/epicodebeservice_v2/api/province?page=0&size=20&sort=id,ASC'
  constructor(private http: HttpClient) { }

  /* CRUD PROVINCE */
  leggiTutteComuni(){
      return this.http.get(this.uriApiComuni);
  }

  leggiTutteProvince(){
    return this.http.get(this.uriApiProvince);
  }

  leggiProvinciaById(id: number){
    return this.http.get('https://epicode.online/epicodebeservice_v2/api/province/'+id)
  }

  aggiungiProvincia(nuovaprovincia:any){
    return this.http.post('https://epicode.online/epicodebeservice_v2/api/province',nuovaprovincia)
  }

  eliminaProvincia(id: number){
    return this.http.delete('https://epicode.online/epicodebeservice_v2/api/province/'+id);
  }

  aggiungiComune(nuovocomune:any){
    return this.http.post('https://epicode.online/epicodebeservice_v2/api/comuni', nuovocomune)
  }

  eliminaComune(id: number){
    return this.http.delete('https://epicode.online/epicodebeservice_v2/api/comuni/'+id)
  }


}
