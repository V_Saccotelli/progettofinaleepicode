import { SignuploginService } from './signuplogin.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RoutguardService implements CanActivate {
  private login = false;
  constructor(private loginservice: SignuploginService,
    private ruoter: Router ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.loginApp();
    return this.login;
  }

  loginApp(){
   if(localStorage.getItem("risposta") !== null){
     this.login = true;
     console.log('è vero')
   }else{
    this.login = false;
    this.ruoter.navigate(['login'])
   }
  }

}

