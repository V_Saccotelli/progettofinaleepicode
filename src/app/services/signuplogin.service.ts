import { Signup } from './../interfaces/signup';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignuploginService {

  constructor(private http: HttpClient) { }

  signUp(newuser: any){
    return this.http.post('https://epicode.online/epicodebeservice_v2/api/auth/signup', newuser)
  }

  login(loguser: any){
    return this.http.post('https://epicode.online/epicodebeservice_v2/api/auth/login', loguser)
  }
}
