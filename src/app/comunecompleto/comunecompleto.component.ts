import { ProvinceComuniService } from './../services/province-comuni.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comunecompleto',
  templateUrl: './comunecompleto.component.html',
  styleUrls: ['./comunecompleto.component.css']
})
export class ComunecompletoComponent implements OnInit {

  provID!:any;
  nomeComune!:any;

  tuttiComuni!: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private prvincecomuniService: ProvinceComuniService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => 
      this.provID = params.id)

      this.prvincecomuniService.leggiTutteComuni()
      .subscribe(resp => this.tuttiComuni = resp);
  }

  creaNuovoComune(){
    let obj = {
      nome: this.nomeComune,
      provincia: {
          id: this.provID
      }
  }
   this.prvincecomuniService.aggiungiComune(obj)
   .subscribe(resp => {
    console.log(resp)
    this.router.navigate(['home'])
   }
   )

/*    alert('comune aggiunto')

   this.router.navigate(['nuovocliente']); */
  }

  deleteComune(item: any){
    this.prvincecomuniService.eliminaComune(item.id)
    .subscribe(resp => console.log(resp))
  }

}
