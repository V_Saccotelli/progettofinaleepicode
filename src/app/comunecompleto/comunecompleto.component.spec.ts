import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComunecompletoComponent } from './comunecompleto.component';

describe('ComunecompletoComponent', () => {
  let component: ComunecompletoComponent;
  let fixture: ComponentFixture<ComunecompletoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComunecompletoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComunecompletoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
