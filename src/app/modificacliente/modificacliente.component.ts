import { ProvinceComuniService } from './../services/province-comuni.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientiService } from '../services/clienti.service';

@Component({
  selector: 'app-modificacliente',
  templateUrl: './modificacliente.component.html',
  styleUrls: ['./modificacliente.component.css']
})
export class ModificaclienteComponent implements OnInit {

  clienteDaModificare!:any;
  province!:any;
  comuni!:any;
  tipocliente!:any;
  constructor(private clientiService: ClientiService,
    private serviceComuni: ProvinceComuniService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
      this.route.params.subscribe(params => this.clientiService.leggiClienteById(params.id)
      .subscribe(resp => this.clienteDaModificare = resp))

      this.serviceComuni.leggiTutteComuni()
    .subscribe(resp => this.comuni = resp);

    this.clientiService.tipiClienti()
    .subscribe(resp => this.tipocliente = resp);
  }

  modificaCliente(){
    confirm('Sicuro/a delle tue modifiche?')
    this.clientiService.updateCliente(this.clienteDaModificare)
    .subscribe(resp => console.log(resp))
    this.router.navigate(['clienti'])
  }

 /*  leggiProvince(){
    this.serviceProvince.leggiTutteProvince()
    .subscribe(resp => this.province = resp);
  }
 */
}
