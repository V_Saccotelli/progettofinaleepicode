import { Router } from '@angular/router';
import { ProvinceComuniService } from './../services/province-comuni.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nuovocomune',
  templateUrl: './nuovocomune.component.html',
  styleUrls: ['./nuovocomune.component.css']
})
export class NuovocomuneComponent implements OnInit {
nomeprovincia!:any;
siglaprovincia!:any;

province!:any
provinceID!:any;
nomecomune!:any;
  constructor(private provincecomuniService: ProvinceComuniService,
    private router: Router) { }

  ngOnInit(): void {
    this.provincecomuniService.leggiTutteProvince()
    .subscribe(resp => {
      this.province = resp;
      console.log(resp)
     
    });
  }

  creaProvincia(){
    let obj =     {
      nome: this.nomeprovincia ,
      sigla: this.siglaprovincia
  }
     this.provincecomuniService.aggiungiProvincia(obj)
     .subscribe(resp => console.log(resp))

     this.router.navigate(['home'])
  }

  deleteProvincia(item: any){
     this.provincecomuniService.eliminaProvincia(item.id)
     .subscribe(resp => console.log(resp))
  }

  creaComune(item:any){
     this.router.navigate(['comune', item.id, 'completo'])
  }

}
