import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuovocomuneComponent } from './nuovocomune.component';

describe('NuovocomuneComponent', () => {
  let component: NuovocomuneComponent;
  let fixture: ComponentFixture<NuovocomuneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuovocomuneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuovocomuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
