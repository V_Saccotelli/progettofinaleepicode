import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FatturesingoloclienteComponent } from './fatturesingolocliente.component';

describe('FatturesingoloclienteComponent', () => {
  let component: FatturesingoloclienteComponent;
  let fixture: ComponentFixture<FatturesingoloclienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FatturesingoloclienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FatturesingoloclienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
