import { FattureService } from './../services/fatture.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-fatturesingolocliente',
  templateUrl: './fatturesingolocliente.component.html',
  styleUrls: ['./fatturesingolocliente.component.css']
})
export class FatturesingoloclienteComponent implements OnInit {

  clienteById!:any;
  fattureCount!: any;
  clienteId!:any;

  data!:any;
  importo!:any;
  stato!:any;
  annoFatture!:any;

  primadata!:any;
  secondadata!:any;

  importouno!:any;
  importodue!:any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private fattureService: FattureService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params =>{
      this.clienteId = params.id;
      this.fattureService.leggiFattureCliente(params.id)
      .subscribe(response => {
        console.log(response)
        this.clienteById = response
        this.fattureCount = new Array(Math.ceil(this.clienteById.totalElements / 10));
      })})
  }

  getPaginations(pagination:any){
    console.log(this.clienteId)
    this.fattureService.leggiFattureCliente(this.clienteId)
    
    .subscribe(resp => {
      console.log(resp)
      this.fattureCount = resp
    })
  }

  eliminaFattura(item: any, clienteId: any){
   this.fattureService.eliminaFattura(item.id)
   .subscribe(resp => {
     console.log(resp)
     //this.fattureService.leggiFattureCliente(item.id, this.clienteId)
     this.router.navigate(['cliente',this.clienteId,'fattura'])
   })
  }

  modificaFattura(item: any){
    this.router.navigate(['cliente',item.id,'modifica','fattura'])
  }

  aggiungiFattura(){
    let objFattura = {
    data:this.data,
    numero:0,
    anno:2019,
    importo: this.importo,
    stato:{
      id: 1,
      nome: this.stato
  },
  cliente:{id:this.clienteId}}
   this.fattureService.creaFattura(objFattura)
   .subscribe(resp => console.log(resp))
   this.importo='';
   this.stato= '';
  }

  cercaTramiteAnno(){
    this.fattureService.cercaByAnno(this.annoFatture)
    .subscribe(resp => {
      console.log(resp)
      this.clienteById = resp
    })
  }

 cercaTramiteDate(){
   this.fattureService.cercaTramiteDate(this.primadata, this.secondadata)
   .subscribe(resp => {
     console.log(resp)
     this.clienteById = resp
  })
 }

 cercaTramiteImporto(){
   this.fattureService.cercaTramiteImporto(this.importouno, this.importodue)
   .subscribe(resp => {
     console.log(resp)
     this.clienteById = resp
   })
 }

}
