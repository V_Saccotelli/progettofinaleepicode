import { RoutguardService } from './services/routeguard.service';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ComunecompletoComponent } from './comunecompleto/comunecompleto.component';
import { NuovocomuneComponent } from './nuovocomune/nuovocomune.component';
import { FatturesingoloclienteComponent } from './fatturesingolocliente/fatturesingolocliente.component';
import { TabellafattureComponent } from './tabellafatture/tabellafatture.component';
import { ModificafatturaComponent } from './modificafattura/modificafattura.component';
import { ModificaclienteComponent } from './modificacliente/modificacliente.component';
import { TabellaclientiComponent } from './tabellaclienti/tabellaclienti.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuovoclienteComponent } from './nuovocliente/nuovocliente.component';

const routes: Routes = [

  {
    path: '', redirectTo: 'signup', pathMatch: 'full'
  },
  {path: 'login', component: LoginComponent, },
  {path: 'signup', component: SignupComponent, },
  {path: 'home', component: HomepageComponent, canActivate: [RoutguardService]},
  {path: 'clienti', component: TabellaclientiComponent, canActivate: [RoutguardService]},
  {path: 'nuovocliente', component: NuovoclienteComponent, canActivate: [RoutguardService]},
  {path: 'fatture', component: TabellafattureComponent, canActivate: [RoutguardService]},
  {path: 'cliente/:id/fattura', component: FatturesingoloclienteComponent, canActivate: [RoutguardService]},
  {path: 'cliente/:id/modifica', component: ModificaclienteComponent, canActivate: [RoutguardService]},
  {path: 'nuovocomune', component: NuovocomuneComponent, canActivate: [RoutguardService]},
  {path: 'comune/:id/completo', component: ComunecompletoComponent, canActivate: [RoutguardService]},
  {path: 'cliente/:id/modifica/fattura', component: ModificafatturaComponent, canActivate: [RoutguardService]}
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
