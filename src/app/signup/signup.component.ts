import { SignuploginService } from './../services/signuplogin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  nome!:string;
  cognome!:string;
  username!:string;
  email!:string;
  password!:string;
  constructor(private signupservice: SignuploginService) { }

  ngOnInit(): void {
  }

  creaNuovoUser(){
    let objUser = {
      nome: this.nome,
      cognome: this.cognome,
      username: this.username,
      email: this.email,
      password: this.password,
      role: ["user"]
  }
    this.signupservice.signUp(objUser)
    .subscribe(resp => console.log(resp))
  }

}
