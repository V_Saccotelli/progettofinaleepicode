import { FattureService } from './../services/fatture.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modificafattura',
  templateUrl: './modificafattura.component.html',
  styleUrls: ['./modificafattura.component.css']
})
export class ModificafatturaComponent implements OnInit {

  fatturaDaModificare!:any;
  statoFattura!:any;

  fattura!:any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private serviceFatture: FattureService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.serviceFatture.leggiFatturaById(params.id)
    .subscribe(resp => { 
      console.log(resp)
      this.fatturaDaModificare = resp}
     ))

     this.serviceFatture.leggiStatoFattura()
     .subscribe(resp => this.statoFattura = resp)
  }

  updateFattura(){
    console.log(this.fatturaDaModificare);
    confirm('Sicuro/a delle tue modifiche?')
   this.serviceFatture.modificaFattura(this.fatturaDaModificare)
    .subscribe(resp => {
      console.log(resp) 
      this.router.navigate(['clienti'])
    })
    
  }

}
