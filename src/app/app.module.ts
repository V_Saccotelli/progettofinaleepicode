import { MaterialModule } from './material/material.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { HomepageComponent } from './homepage/homepage.component';
import { FooterComponent } from './footer/footer.component';
import { TabellaclientiComponent } from './tabellaclienti/tabellaclienti.component';
import { NuovoclienteComponent } from './nuovocliente/nuovocliente.component';
import { ModificaclienteComponent } from './modificacliente/modificacliente.component';
import { ModificafatturaComponent } from './modificafattura/modificafattura.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyHttpInterceptor } from './_interceptor/my-http-interceptor';
import { TabellafattureComponent } from './tabellafatture/tabellafatture.component';
import { FatturesingoloclienteComponent } from './fatturesingolocliente/fatturesingolocliente.component';
import { NuovafatturaComponent } from './nuovafattura/nuovafattura.component';
import { NuovocomuneComponent } from './nuovocomune/nuovocomune.component';
import { ComunecompletoComponent } from './comunecompleto/comunecompleto.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomepageComponent,
    FooterComponent,
    TabellaclientiComponent,
    NuovoclienteComponent,
    ModificaclienteComponent,
    ModificafatturaComponent,
    TabellafattureComponent,
    FatturesingoloclienteComponent,
    NuovafatturaComponent,
    NuovocomuneComponent,
    ComunecompletoComponent,
    SignupComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule
    
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: MyHttpInterceptor,
    multi: true
  }
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
